#!/bin/env python3
# pylint: disable=bad-continuation
# pylint: disable=bad-whitespace
# pylint: disable=too-few-public-methods
# pylint: disable=too-many-arguments
# pylint: disable=dangerous-default-value
# pylint: disable=invalid-name
"""
Convert SVABX configuration to the new and fancy SLANG
"""

import sys
import logging
import datetime
from pathlib import Path

import shutil
import slang
import svabx
import rendering

def setup_custom_logger(name):
    """
    Create custom logger
    """
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    handler = logging.FileHandler('log.txt', mode='w')
    handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.addHandler(screen_handler)

    return logger

# Create logger
LOG = setup_custom_logger("SVABCONVERT")

def create_file(name, svabxfile, renderer, slangobj, parsed):
    """
    Create a single file
    """
    filename = "output/" + name
    LOG.debug("Creating file '%s'", filename)

    now = datetime.datetime.now()
    date = "%04d-%02d-%02d" % (now.year, now.month, now.day)

    fp = open(filename, "wb")
    renderer(fp, svabxfile, date, slangobj, parsed)
    fp.close()

def create_file_per_node(svabxfile, renderer, slangobj, parsed):
    """
    Create multiple files per node
    """
    now = datetime.datetime.now()
    date = "%04d-%02d-%02d" % (now.year, now.month, now.day)

    for n in parsed.nodes():
        filename = "output/" + n.program
        LOG.debug("Creating file '%s'", filename)

        fp = open(filename, "wb")
        renderer(fp, svabxfile, date, slangobj, n)
        fp.close()

# Check arguments
if len(sys.argv) < 2:
    sys.stdout.write("Usage: convert.py <svabxfile>\n")
    sys.exit(1)

# File minus path
INPUT_FILE = Path(sys.argv[1]).name

# Remove everything after the last "."
OUTPUT_PREFIX = ".".join(INPUT_FILE.split(".")[:-1])

# Config file
CONFIG_FILE = OUTPUT_PREFIX + ".scfg"

# SLANG file
MAIN_FILE = "Main.slg"

# Parse
PARSED = svabx.SVABX(sys.argv[1], OUTPUT_PREFIX)

# Create a SLANG object
SLANG = slang.SLANG(CONFIG_FILE)

# Update file
UPDATE_FILE = "UpdateFW.slg"

# Render Main.slg
create_file(MAIN_FILE, INPUT_FILE, rendering.render_main, SLANG, PARSED)

# Render update file
create_file(UPDATE_FILE, INPUT_FILE, rendering.render_update, SLANG, PARSED)

# Render configuration
create_file(CONFIG_FILE, INPUT_FILE, rendering.render_configuration, SLANG,
        PARSED)

# Render update firwmare files, a little bit special
create_file_per_node(INPUT_FILE, rendering.render_firmware, SLANG, PARSED)

