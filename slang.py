# pylint: disable=invalid-name
# pylint: disable=too-few-public-methods
# pylint: disable=no-self-use
# pylint: disable=bare-except
# pylint: disable=too-many-arguments
# pylint: disable=dangerous-default-value
# pylint: disable=bad-continuation

"""
SLANG file represenation
"""

class SLANG:
    """
    Class representing SLANG meta-data
    """
    version = "0,9"
    update = "UpdateFW.slg"

    def __init__(self, configfile):
        self.configfile = configfile.split("/")[-1]

class SDO:
    """
    Class representing an SDO
    """
    index = None
    subindex = None
    size = None
    attributes = None
    data = None
    offset = None
    name = None

    def __init__(self, index, subindex, size, attributes, data, name=None,
            offset=None):
        self.name = name
        self.index = index
        self.subindex = subindex
        self.size = size
        self.attributes = attributes
        self.data = data
        self.offset = offset

    @property
    def data_f(self):
        """
        Get the data in a "correct" type
        """
        tmp = self.data.strip('"')
        out = None

        # If the SDO is text, it's a string
        if self.attributes and 'T' in self.attributes:
            return str(self.data)

        # Not a string, test if we can use it as an integer
        try:
            out = int(tmp, 16)
            return int(out)
        except ValueError:
            # Nope, didnt work
            out = None

        # Ok, so it's not text, or can be converted to integer directly
        # Try splitting it up
        tmp = tmp.split(" ")

        if len(tmp) == 1:
            # It cannot be split, we give up just return
            return self.data

        # Out should a list/array
        out = []
        for t in tmp:
            # Integer e
            out.append(int(t, 16))

        # Finally!
        return out

class Software:
    """
    Class representing a software
    """
    name = None
    index = None
    subindex = None
    size = None
    offset = None
    bootloader = False
    path = ""
    hexfile = ""

    def __init__(self, name, index, subindex, size, offset, bootloader, path):
        self.name = name
        self.index = index
        self.subindex = subindex
        self.size = size
        self.offset = offset
        self.bootloader = bootloader

        filename = path.strip('""').split("\\")[-1]
        binfile = ".".join(filename.split(".")[:-1]) + ".bin"
        self.path = '"%s"' % (binfile)
        self.hexfile = path

    @classmethod
    def from_sdo(cls, sdo):
        """
        Create a Software from an SDO
        """
        boot = True if 'B' in sdo.attributes else False
        return cls(sdo.name, sdo.index, sdo.subindex, sdo.size, sdo.offset,
                boot, sdo.data)

    def __str__(self):
        boot = "Boot" if self.bootloader else "App"

        return "Software: %s (%04X/%02X)[%d+%d] %s '%s'" % (self.name,
                self.index, self.subindex, self.size, self.offset, boot,
                self.path)

class Identification:
    """
    Class representing an identification
    """
    name = ""
    index = 0
    subindex = 0
    size = 0
    boot = False
    data = ""

    def __init__(self, name, index, subindex, size, boot, data):
        self.name = name
        self.index = index
        self.subindex = subindex
        self.size = size
        self.boot = boot
        self.data = data

    def __str__(self):
        flag = "Boot" if self.boot else "App"
        return "Ident: (%s) %s (%04X/%02X) %s" % (flag, self.name,
                self.index, self.subindex, self.data)

    @classmethod
    def from_sdo(cls, sdo):
        """
        Create identification object from an SDO
        """
        if 'E' in sdo.attributes:
            boot = True if 'B' in sdo.attributes else False
            return cls(sdo.name, sdo.index, sdo.subindex, sdo.size, boot,
                    sdo.data)

        return None

    @property
    def data_formatted(self):
        """
        Get data, formatted by length
        """
        return self.data.strip('"')

class Profile:
    """
    Class representing a profile
    """
    number = None
    sdos = []

    def __init__(self, number, sdos=[]):
        self.number = number
        self.sdos = sdos

    def __str__(self):
        return "Profile[%d]: # SDOs[%d]" % (self.number, len(self.sdos))

class Node:
    """
    Class representing a node
    """
    nodeid = None
    profiles = []
    program = ""
    firmwares = []
    identifications = []
    softwares = []

    def __init__(self, prefix, nodeid, profiles=[]):
        self.nodeid = nodeid
        self.profiles = profiles
        self.program = prefix + "_prog_%d.slg" % (self.nodeid)

    def __str__(self):
        return "Node[%d] : # Profiles[%d" % (self.nodeid, len(self.profiles))

    @property
    def ident_applications(self):
        """
        Get all idents for applications
        """
        return filter(lambda x: False if x.boot else True, self.identifications)

    @property
    def ident_bootloaders(self):
        """
        Get all idents bootloaders
        """
        return filter(lambda x: True if x.boot else False, self.identifications)

    @property
    def applications(self):
        """
        Get all applications
        """
        return filter(lambda x: False if x.bootloader else True, self.softwares)

    @property
    def bootloaders(self):
        """
        Get all bootloaders
        """
        return filter(lambda x: True if x.bootloader else False, self.softwares)


