# pylint: disable=invalid-name
# pylint: disable=too-few-public-methods
# pylint: disable=no-self-use
# pylint: disable=bare-except
# pylint: disable=bad-continuation

"""
Wrapper/Parser for Input files in SVABX format
"""

from lxml import objectify
import slang

class SVABX:
    """
    Python representation of a SVABX file
    """
    root = None
    prefix = None

    def __init__(self, filename, prefix):
        self.prefix = prefix

        fp = open(filename, "r")
        xml = fp.read()
        xml = xml.encode('utf-8')
        self.root = objectify.fromstring(xml)

    def sdo(self, e):
        """
        Parse all SDO from an element
        """
        sdos = []
        for s in e.SDO:
            index = int(s.attrib['Index'], 16)
            subindex = int(s.attrib['SubIndex'], 16)
            size = int(s.attrib['Size'], 16)
            data = '"%s"' % s.attrib['Data']

            try:
                attribute = '"%s"' % s.attrib['Attribute']
                attribute = attribute.replace('\\', '\\\\')
            except:
                attribute = None

            try:
                offset = int(s.attrib['Offset'], 16)
            except:
                offset = None

            try:
                name = s.attrib['Name']
            except:
                name = None

            sdo = slang.SDO(index, subindex, size, attribute, data,
                    name, offset)
            sdos.append(sdo)

        return sdos

    def common_profile(self, e):
        """
        Parse a common profile from a node element
        """
        sdos = self.sdo(e)
        return slang.Profile(-1, sdos)

    def profiles(self, e):
        """
        Parse all profiles from a profile settings element
        """
        profiles = []
        for ps in e.ProfileSettings:
            p = slang.Profile(int(ps.attrib['ProfileNumber']), [])

            # MACSTool kludge
            if p.number > 0:
                p.number = p.number -1

            p.sdos = self.sdo(ps)
            profiles.append(p)

        return profiles

    def identification(self, e):
        """
        Parse the identification element from a config
        """
        identification = []

        # Create SDO:s from Identification block
        sdos = self.sdo(e.Identification)
        for s in sdos:
            if 'E' in s.attributes:
                identification.append(slang.Identification.from_sdo(s))

        return identification

    def softwares(self, e):
        """
        Parse the software block from a SVABX file
        """
        softwares = []

        # Read all SDO from the Software part
        sdos = self.sdo(e.Software)

        for s in sdos:
            softwares.append(slang.Software.from_sdo(s))

        return softwares

    def nodes(self):
        """
        Get all Nodes in XML tree format
        """
        nodes = []
        for e in self.root.Node:
            node = slang.Node(self.prefix, int(e.attrib['Id']))

            # Get the common profile and put it in the beginning
            cp = self.common_profile(e.ProfileCommonSettings)
            node.profiles.append(cp)

            # Get all profiles and put them in the node
            node.profiles += self.profiles(e)

            # Get all softwares and put them in the node
            node.softwares = self.softwares(e)

            # Get all relevant identifications
            node.identifications = self.identification(e)

            nodes.append(node)

        return nodes
