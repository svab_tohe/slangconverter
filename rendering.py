# pylint: disable=bad-continuation
# pylint: disable=bad-whitespace
# pylint: disable=trailing-newlines

"""
Rendering module. Renders various files for the convertion. SLANG, config et
cetera
"""

import os
import logging
import json
import msgpack
import jinja2

#
# THE ENVIRONMENT
#
SLANG_JINJA_ENV = jinja2.Environment(
        block_start_string = '{%',
        block_end_string = '%}',
        variable_start_string = '{{',
        variable_end_string = '}}',
        comment_start_string = '{#',
        comment_end_string = '#}',
        line_statement_prefix = '%%',
        line_comment_prefix = '%#',
        trim_blocks = True,
        autoescape = False,
        loader = jinja2.FileSystemLoader(os.path.abspath('.'))
)

LOG = logging.getLogger("SVABCONVERT")

def render_configuration(out, svabxfile, date, slangobj, parsed):
    """
    Render a configuration from a list of nodes
    """
    # Ignored
    svabxfile = svabxfile
    date = date

    LOG.debug("Rendering configuration...")

    # Get a template
    template = SLANG_JINJA_ENV.get_template('templates/SlangConfigTemplate.json')

    # Render to JSON
    jsondata = template.render(slang=slangobj, nodes=parsed.nodes())

    # Parse JSON as dictionary
    parsed = json.loads(jsondata)

    # Msgpack JSON to output file
    msgpack.pack(parsed, out)

    LOG.debug("Done")

def render_main(out, svabxfile, date, slangobj, parsed):
    """
    Render a set of SLANG scripts given the software and identification
    """
    LOG.debug("Rendering SLANG script 'Main.slg'...")

    # Get a template
    template = SLANG_JINJA_ENV.get_template('templates/MainTemplate.slg')

    # Render to Slang
    script = template.render(slang=slangobj, file=svabxfile, date=date,
            nodes=parsed.nodes())

    # Save to file
    out.write(bytes(script, 'ascii'))

    LOG.debug("Done")

def render_update(out, svabxfile, date, slangobj, parsed):
    """
    Render the update file
    """
    LOG.debug("Rendering shared update file...")

    # Get the template
    template = SLANG_JINJA_ENV.get_template('templates/UpdateNodeTemplate.slg')

    # Render to Slang
    script = template.render(slang=slangobj, file=svabxfile, date=date,
            nodes=parsed.nodes())

    # Save to file
    out.write(bytes(script, 'ascii'))

    LOG.debug("Done")

def render_firmware(out, svabxfile, date, slangobj, node):
    """
    Render the firwmare file
    """
    LOG.debug("Rendering firmware file...")

    # Get the template
    template = SLANG_JINJA_ENV.get_template('templates/UpdateFWTemplate.slg')

    # Render to Slang
    script = template.render(slang=slangobj, file=svabxfile, date=date,
            node=node)

    # Save to file
    out.write(bytes(script, 'ascii'))

    LOG.debug("Done")
